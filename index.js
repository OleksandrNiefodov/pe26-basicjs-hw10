/*
Завдання
Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?

- для створення нових елементів можна використовувати: 
createElement()
createTextNode()
або можна виористати універсальний метод:
insertAdjacentElement()

для додавання якщо викоистовуємо createElement() можемов використати:

append(child) 
prepend(child) 
before(child) 
after(child) 
replaceWith(child)

у випадку з insertAdjacentElement() вставку ми записуємо як перший параметр в дужках:

beforebegin
afterbegin
beforeend 
afterend

2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

- для початку нам потрібно знайти цей клас за допомогою, наприклад querySelector і записуємо це в змінну
потім використовуємо метод romove() для створеної змінної. 

3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
для createElement() це:
before() 
after()

для insertAdjacentElement() це:
beforebegin
afterend
*/
/*
Практичні завдання
1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
*/

const paragraphText = document.querySelector('.footer__title');
const aTag = document.createElement('a');
aTag.innerText = 'Learn More';
aTag.href = '#';
aTag.style.display = 'block'; 
paragraphText.append(aTag);

/*
2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.
*/

const select = document.createElement('select');

select.id = 'rating';

const features = document.querySelector('.features');
features.before(select);


for(let i = 4; i >= 1; i--){
    const option = document.createElement('option');
    option.value = i;
    option.innerText = `${i} Star${i > 1 ? 's' : ''}`;
    select.append(option);
};

// const optionFour = document.createElement('option');
// optionFour.innerText = '4 Star';
// select.append(optionFour);

// const optionThree = document.createElement('option');
// optionThree.innerText = '3 Star';
// select.append(optionThree);

// const optionTwo = document.createElement('option');
// optionTwo.innerText = '2 Star';
// select.append(optionTwo);

// const optionOne = document.createElement('option');
// optionOne.innerText = '1 Star';
// select.append(optionOne);
// console.log(select);